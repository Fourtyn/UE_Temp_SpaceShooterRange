// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SSR_GameModeBase.h"
#include "Characters/SSR_Turret.h"
#include "Core/SSR_PlayerController.h"

ASSR_GameModeBase::ASSR_GameModeBase()
{
	DefaultPawnClass = ASSR_Turret::StaticClass();
	PlayerControllerClass = ASSR_PlayerController::StaticClass();
}
