// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SSR_PlayerController.h"
#include "Components/InputComponent.h"

// Debug: Logging
DEFINE_LOG_CATEGORY_STATIC(Log_SSR_PlayerController, All, All)

void ASSR_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Shoot", IE_Pressed, this, &ASSR_PlayerController::Shoot);
	InputComponent->BindAction("ChangeProjectile", IE_Pressed, this, &ASSR_PlayerController::ChangeProjectile);
}

void ASSR_PlayerController::Shoot()
{
	UE_LOG(Log_SSR_PlayerController, Display, TEXT("Shooting"));
}

void ASSR_PlayerController::ChangeProjectile()
{
	UE_LOG(Log_SSR_PlayerController, Display, TEXT("ChangeProjectile"));

	if (Projectiles.Num() <= 1)
		return;
	
	/*
	ProjType* CurrentProjectile = Cast<ProjType>(Projectiles[CurrentProjectileIndex]);
	int PreviousProjectileIndex = CurrentProjectileIndex;
	CurrentProjectileIndex = (CurrentProjectileIndex + 1) % Projectiles.Num();


	// ����� ������ ��������� � �������
	if(CurrentProjectileIndex == PreviousProjectileIndex)
		return;

	// if CurrProjectile == null => return;
	if(!CurrentProjectile)
		return;
	*/

}
