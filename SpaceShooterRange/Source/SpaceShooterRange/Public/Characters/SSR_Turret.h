// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SSR_Turret.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class SPACESHOOTERRANGE_API ASSR_Turret : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASSR_Turret();

	UPROPERTY(VisibleAnywhere)
		USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* BarrelStaticMeshComponent;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere)
		float Velocity = 30.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArmComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	// Input functions
	UFUNCTION()
	void TurnRight(float Value);
	UFUNCTION()
	void TurnUp(float Value);

	void Shoot();
};
