// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SSR_GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHOOTERRANGE_API ASSR_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASSR_GameModeBase();
};
