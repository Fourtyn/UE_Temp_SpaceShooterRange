// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SSR_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHOOTERRANGE_API ASSR_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void SetupInputComponent() override;

private:
	TArray<int> Projectiles; // Placeholder for different Projectile types

	int CurrentProjectileIndex = 0;

	void Shoot();
	void ChangeProjectile();
};
